package se.kodaren.messageboard.security;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.hateoas.mediatype.MessageResolver;
import org.springframework.hateoas.mediatype.hal.CurieProvider;
import org.springframework.hateoas.mediatype.hal.Jackson2HalModule;
import org.springframework.hateoas.server.core.AnnotationLinkRelationProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import se.kodaren.messageboard.api.user.UserController;
import se.kodaren.messageboard.api.user.model.UserPOST;
import se.kodaren.messageboard.repository.UserRepository;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static se.kodaren.messageboard.security.SecurityConstants.*;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;

    public AuthenticationFilter(AuthenticationManager authenticationManager, UserRepository userRepository) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        setFilterProcessesUrl("/api/login");
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        try {
            var creds = new ObjectMapper().readValue(request.getInputStream(), UserPOST.class);

            var usernamePasswordToken = new UsernamePasswordAuthenticationToken(
                    creds.getUsername(),
                    creds.getPassword(),
                    new ArrayList<>());
            return authenticationManager.authenticate(usernamePasswordToken);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain chain, Authentication authentication) {
        final var username = ((User) authentication.getPrincipal()).getUsername();
        String token = JWT.create()
                .withSubject(username)
                .withExpiresAt(getExpiresAt())
                .sign(HMAC512(SECRET.getBytes()));
        response.addHeader(HttpHeaders.AUTHORIZATION, BEARER + token);
        try {
            // Workaround to correctly map HATEOAS links
            ObjectMapper mapper = new ObjectMapper();
            Jackson2HalModule module = new Jackson2HalModule();
            mapper.registerModule(module);
            mapper.setHandlerInstantiator(new Jackson2HalModule.HalHandlerInstantiator(new AnnotationLinkRelationProvider(), CurieProvider.NONE, MessageResolver.DEFAULTS_ONLY));

            var userDB = userRepository.findByUsername(username).orElseThrow();
            se.kodaren.messageboard.api.user.model.User user = new se.kodaren.messageboard.api.user.model.User();
            user.setId(userDB.getId());
            user.setUsername(userDB.getUsername());
            user.add(linkTo(methodOn(UserController.class).getUser(user.getId(), null)).withSelfRel());

            String responseBody = mapper.writeValueAsString(user);
            response.getWriter().write(responseBody);
            response.setContentType(APPLICATION_JSON_VALUE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Date getExpiresAt() {
        LocalDateTime expiresAt = LocalDateTime.now().plusMinutes(EXPIRATION_TIME);
        return Date.from(expiresAt.atZone(ZoneId.systemDefault()).toInstant());
    }
}
