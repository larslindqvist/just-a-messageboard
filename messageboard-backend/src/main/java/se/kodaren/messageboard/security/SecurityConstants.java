package se.kodaren.messageboard.security;

class SecurityConstants {
    static final String SECRET = "TheSecreetToUseWhenGeneratingJWT";
    static final long EXPIRATION_TIME = 10;
    static final String BEARER = "Bearer ";
}
