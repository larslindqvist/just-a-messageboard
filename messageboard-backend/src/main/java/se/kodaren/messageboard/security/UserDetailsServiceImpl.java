package se.kodaren.messageboard.security;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import se.kodaren.messageboard.repository.UserDB;
import se.kodaren.messageboard.repository.UserRepository;

import static java.util.Collections.emptyList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDB userDB = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
        return new User(userDB.getUsername(), userDB.getPassword(), emptyList());
    }
}
