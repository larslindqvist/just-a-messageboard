package se.kodaren.messageboard.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<UserDB, Long> {
    Optional<UserDB> findByUsername(String username);
    boolean existsByUsername(String username);
    UserDB getById(Long id);
}
