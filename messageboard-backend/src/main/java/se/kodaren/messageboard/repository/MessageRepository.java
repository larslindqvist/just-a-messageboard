package se.kodaren.messageboard.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MessageRepository extends CrudRepository<MessageDB, Long> {
    List<MessageDB> findByBoardId(Long boardId);
}
