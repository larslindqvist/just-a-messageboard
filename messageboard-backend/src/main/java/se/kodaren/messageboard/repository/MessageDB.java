package se.kodaren.messageboard.repository;

import javax.persistence.*;

@Entity
public class MessageDB {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String message;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private BoardDB board;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private UserDB user;

    public UserDB getUser() {
        return user;
    }

    public void setUser(UserDB userDB) {
        this.user = userDB;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BoardDB getBoard() {
        return board;
    }

    public void setBoard(BoardDB boardDB) {
        this.board = boardDB;
    }

    public Long getId() {
        return id;
    }
}
