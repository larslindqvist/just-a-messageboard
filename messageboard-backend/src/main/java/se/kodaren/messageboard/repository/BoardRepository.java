package se.kodaren.messageboard.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BoardRepository extends CrudRepository<BoardDB, Long> {
    BoardDB getById(Long id);
    List<BoardDB> findByUserId(Long userId);
}
