package se.kodaren.messageboard.repository;

import javax.persistence.*;

@Entity
public class BoardDB {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    private UserDB user;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserDB getUser() {
        return user;
    }

    public void setUser(UserDB userDB) {
        this.user = userDB;
    }

    public Long getId() {
        return id;
    }
}
