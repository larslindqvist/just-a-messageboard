package se.kodaren.messageboard.api.board.model;

import org.springframework.hateoas.RepresentationModel;

import java.util.ArrayList;
import java.util.List;

public class Boards extends RepresentationModel<BoardItem> {
    private List<BoardItem> boards = new ArrayList<>();

    public List<BoardItem> getBoards() {
        return boards;
    }

    public void setBoards(List<BoardItem> boards) {
        this.boards = boards;
    }
}
