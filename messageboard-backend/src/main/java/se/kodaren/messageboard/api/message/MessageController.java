package se.kodaren.messageboard.api.message;

import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.kodaren.messageboard.api.message.model.Message;
import se.kodaren.messageboard.api.message.model.MessageItem;
import se.kodaren.messageboard.api.message.model.MessagePOST;
import se.kodaren.messageboard.service.MessageboardService;
import se.kodaren.messageboard.service.exception.BoardNotFoundException;
import se.kodaren.messageboard.service.exception.MessageNotFoundException;
import se.kodaren.messageboard.service.exception.UserNotFoundException;

import javax.validation.Valid;
import java.security.Principal;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/boards/{boardId}/messages")
public class MessageController {
    private final MessageboardService messageboardService;

    public MessageController(MessageboardService messageboardService) {
        this.messageboardService = messageboardService;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CollectionModel<MessageItem>> getMessages(@PathVariable Long boardId) {
        try {
            var messages = messageboardService.getMessages(boardId);
            return ResponseEntity.ok(CollectionModel.of(messages.getMessageItems(), messages.getLinks()));
        }
        catch (BoardNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(value = "/{messageId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Message> getMessage(@PathVariable Long boardId, @PathVariable Long messageId) {
        try {
            Message message = messageboardService.findMessage(boardId, messageId);
            return ResponseEntity.ok(message);
        }
        catch (BoardNotFoundException | MessageNotFoundException e){
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Message> createMessage(@PathVariable Long boardId, @RequestBody @Valid MessagePOST messagePOST, Principal principal) {
        try {
            var user = messageboardService.findUser(principal.getName());
            Message created = messageboardService.createMessage(boardId, user.getId(), messagePOST);
            return ResponseEntity.created(created.getRequiredLink("self").toUri()).build();
        }
        catch (BoardNotFoundException | UserNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(value = "/{messageId}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Message> updateMessage(@PathVariable Long boardId, @PathVariable Long messageId, @RequestBody @Valid MessagePOST message, Principal principal) {
        try {
            var msg = messageboardService.findMessage(boardId, messageId);
            if (msg.getUser().equals(principal.getName())) {
                messageboardService.updateMessage(boardId, messageId, message);
                return ResponseEntity.ok().build();
            }
            else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
        }
        catch (BoardNotFoundException | MessageNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(value = "/{messageId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteMessage(@PathVariable Long boardId, @PathVariable Long messageId, Principal principal) {
        try {
            var message = messageboardService.findMessage(boardId, messageId);
            if (message.getUser().equals(principal.getName())) {
                messageboardService.deleteMessage(boardId, messageId);
                return ResponseEntity.noContent().build();
            }
            else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
        }
        catch (BoardNotFoundException | MessageNotFoundException e){
            return ResponseEntity.notFound().build();
        }
    }
}
