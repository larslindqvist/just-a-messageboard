package se.kodaren.messageboard.api.util;

public class Constants {
    public static final String NOT_EMPTY_PATTERN = "^(?!\\s*$).+";
}
