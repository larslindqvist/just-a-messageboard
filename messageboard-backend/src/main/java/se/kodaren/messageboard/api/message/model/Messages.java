package se.kodaren.messageboard.api.message.model;

import org.springframework.hateoas.RepresentationModel;

import java.util.ArrayList;
import java.util.List;

public class Messages extends RepresentationModel<Messages> {
    private List<MessageItem> messageItems = new ArrayList<>();

    public List<MessageItem> getMessageItems() {
        return messageItems;
    }

    public void setMessageItems(List<MessageItem> messageItems) {
        this.messageItems = messageItems;
    }
}
