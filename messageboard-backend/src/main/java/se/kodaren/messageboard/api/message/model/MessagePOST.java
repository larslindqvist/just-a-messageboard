package se.kodaren.messageboard.api.message.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import static se.kodaren.messageboard.api.util.Constants.NOT_EMPTY_PATTERN;

public class MessagePOST {
    @Pattern(regexp = NOT_EMPTY_PATTERN)
    @NotEmpty
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
