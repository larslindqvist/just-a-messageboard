package se.kodaren.messageboard.api.user.converter;

import org.springframework.stereotype.Component;
import se.kodaren.messageboard.api.user.model.User;
import se.kodaren.messageboard.repository.UserDB;

@Component
public class UserConverter {
    public User convert(UserDB userDB) {
        User user = new User();
        user.setId(userDB.getId());
        user.setUsername(userDB.getUsername());
        return user;
    }
}
