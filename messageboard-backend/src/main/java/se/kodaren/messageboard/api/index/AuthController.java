package se.kodaren.messageboard.api.index;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.kodaren.messageboard.api.user.model.User;
import se.kodaren.messageboard.api.user.model.UserPOST;
import se.kodaren.messageboard.service.MessageboardService;
import se.kodaren.messageboard.service.exception.UserAlreadyExistsException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/register")
public class AuthController {
    private final MessageboardService messageboardService;

    public AuthController(MessageboardService messageboardService) {
        this.messageboardService = messageboardService;
    }

    @PostMapping(produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<User> register(@RequestBody UserPOST userPOST) {
        try {
            User created = messageboardService.createUser(userPOST);
            return ResponseEntity.created(created.getRequiredLink("self").toUri()).build();
        }
        catch (UserAlreadyExistsException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }
}
