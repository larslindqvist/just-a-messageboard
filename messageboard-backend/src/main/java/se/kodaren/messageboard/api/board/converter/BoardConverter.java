package se.kodaren.messageboard.api.board.converter;

import org.springframework.stereotype.Component;
import se.kodaren.messageboard.api.board.model.Board;
import se.kodaren.messageboard.api.board.model.BoardItem;
import se.kodaren.messageboard.repository.BoardDB;

@Component
public class BoardConverter {
    public Board convert(BoardDB boardDB) {
        var b = new Board();
        b.setId(boardDB.getId());
        b.setName(boardDB.getName());
        b.setUser(boardDB.getUser().getUsername());
        return b;
    }

    public BoardItem convertItem(BoardDB boardDB) {
        var b = new BoardItem();
        b.setId(boardDB.getId());
        b.setName(boardDB.getName());
        b.setUser(boardDB.getUser().getUsername());
        return b;
    }
}
