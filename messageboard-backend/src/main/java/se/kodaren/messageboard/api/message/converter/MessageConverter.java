package se.kodaren.messageboard.api.message.converter;

import org.springframework.stereotype.Component;
import se.kodaren.messageboard.api.message.model.Message;
import se.kodaren.messageboard.api.message.model.MessageItem;
import se.kodaren.messageboard.repository.MessageDB;

@Component
public class MessageConverter {
    public Message convert(MessageDB messageDB) {
        var m = new Message();
        m.setId(messageDB.getId());
        m.setMessage(messageDB.getMessage());
        m.setUser(messageDB.getUser().getUsername());
        return m;
    }

    public MessageItem convertItem(MessageDB messageDB) {
        var m = new MessageItem();
        m.setId(messageDB.getId());
        m.setMessage(messageDB.getMessage());
        m.setUser(messageDB.getUser().getUsername());
        return m;
    }
}
