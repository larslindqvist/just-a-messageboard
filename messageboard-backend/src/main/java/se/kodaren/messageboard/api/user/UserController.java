package se.kodaren.messageboard.api.user;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.kodaren.messageboard.api.user.model.User;
import se.kodaren.messageboard.service.MessageboardService;
import se.kodaren.messageboard.service.exception.UserNotFoundException;

import java.security.Principal;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final MessageboardService messageboardService;

    public UserController(MessageboardService messageboardService) {
        this.messageboardService = messageboardService;
    }

    @GetMapping(value = "/{userId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@PathVariable Long userId, Principal principal) {
        try {
            var user = messageboardService.findUser(userId);
            if (principal != null && user.getUsername().equals(principal.getName())) {
                return ResponseEntity.ok(user);
            }
            else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
        }
        catch (UserNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(value = "/{userId}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long userId, Principal principal) {
        try {
            var user = messageboardService.findUser(userId);
            if (user.getUsername().equals(principal.getName())) {
                messageboardService.deleteUser(userId);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
        }
        catch (UserNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
