package se.kodaren.messageboard.api.user.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import static se.kodaren.messageboard.api.util.Constants.NOT_EMPTY_PATTERN;

public class UserPOST {
    @Pattern(regexp = NOT_EMPTY_PATTERN)
    @NotEmpty
    private String username;
    @Pattern(regexp = NOT_EMPTY_PATTERN)
    @NotEmpty
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
