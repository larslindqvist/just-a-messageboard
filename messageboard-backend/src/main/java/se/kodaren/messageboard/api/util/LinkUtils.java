package se.kodaren.messageboard.api.util;

import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;
import se.kodaren.messageboard.api.board.BoardController;
import se.kodaren.messageboard.api.board.model.Board;
import se.kodaren.messageboard.api.board.model.Boards;
import se.kodaren.messageboard.api.index.AuthController;
import se.kodaren.messageboard.api.index.IndexController;
import se.kodaren.messageboard.api.index.model.Index;
import se.kodaren.messageboard.api.message.MessageController;
import se.kodaren.messageboard.api.message.model.Message;
import se.kodaren.messageboard.api.message.model.Messages;
import se.kodaren.messageboard.api.user.UserController;
import se.kodaren.messageboard.api.user.model.User;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class LinkUtils {
    public void addLinks(User user) {
        user.add(linkTo(methodOn(UserController.class).getUser(user.getId(), null)).withSelfRel());
    }

    public void addLinks(Boards boards) {
        boards.add(linkTo(methodOn(BoardController.class).getBoards()).withSelfRel());

        boards.getBoards().forEach(i -> i.add(linkTo(methodOn(BoardController.class).getBoard(i.getId())).withRel("via")));
    }

    public void addLinks(Board board) {
        board.add(linkTo(methodOn(BoardController.class).getBoard(board.getId())).withSelfRel());
        board.add(linkTo(methodOn(MessageController.class).getMessages(board.getId())).withRel("collection"));
    }

    public void addLinks(Long boardId, Messages messages) {
        messages.add(linkTo(methodOn(MessageController.class).getMessages(boardId)).withSelfRel());
        messages.getMessageItems().forEach(i -> i.add(linkTo(methodOn(MessageController.class).getMessage(boardId, i.getId())).withRel("via")));
    }

    public void addLinks(Long boardId, Message message) {
        message.add(linkTo(methodOn(MessageController.class).getMessage(boardId, message.getId())).withSelfRel());
    }

    public void addLinks(Index index, boolean aunthenticated) {
        // Index
        Link indexSelf = linkTo(methodOn(IndexController.class).getIndex(null)).withSelfRel();
        index.add(indexSelf);

        // Register and login
        index.add(linkTo(methodOn(AuthController.class).register(null)).withRel("register"));
        index.add(Link.of(indexSelf.getHref() + "/login").withRel("login"));

        // Additional links that authenticated users can access
        if (aunthenticated) {
            // Boards
            index.add(linkTo(methodOn(BoardController.class).getBoards()).withRel("collection").withName("Boards"));
        }
    }
}
