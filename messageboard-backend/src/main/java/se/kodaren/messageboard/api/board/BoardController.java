package se.kodaren.messageboard.api.board;

import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.kodaren.messageboard.api.board.model.Board;
import se.kodaren.messageboard.api.board.model.BoardItem;
import se.kodaren.messageboard.api.board.model.BoardPOST;
import se.kodaren.messageboard.service.MessageboardService;
import se.kodaren.messageboard.service.exception.BoardNotFoundException;
import se.kodaren.messageboard.service.exception.UserNotFoundException;

import javax.validation.Valid;
import java.security.Principal;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/boards")
public class BoardController {
    private final MessageboardService messageboardService;

    public BoardController(MessageboardService messageboardService) {
        this.messageboardService = messageboardService;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public CollectionModel<BoardItem> getBoards() {
        var boards = messageboardService.getBoards();
        return CollectionModel.of(boards.getBoards(), boards.getLinks());
    }

    @GetMapping(value = "/{boardId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Board> getBoard(@PathVariable Long boardId) {
        try {
            var board = messageboardService.findBoard(boardId);
            return ResponseEntity.ok(board);
        }
        catch (BoardNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Board> createBoard(@RequestBody @Valid BoardPOST boardPOST, Principal principal) {
        var user = messageboardService.findUser(principal.getName());
        var created = messageboardService.createUserBoard(user.getId(), boardPOST);
        return ResponseEntity.created(created.getRequiredLink("self").toUri()).build();
    }
}
