package se.kodaren.messageboard.api.board.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import static se.kodaren.messageboard.api.util.Constants.NOT_EMPTY_PATTERN;

public class BoardPOST {
    @Pattern(regexp = NOT_EMPTY_PATTERN)
    @NotEmpty
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
