package se.kodaren.messageboard.api.user.model;

import org.springframework.hateoas.RepresentationModel;

public class User extends RepresentationModel<User> {
    private Long id;
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
