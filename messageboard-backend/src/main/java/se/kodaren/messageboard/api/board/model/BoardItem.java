package se.kodaren.messageboard.api.board.model;

import org.springframework.hateoas.RepresentationModel;

public class BoardItem extends RepresentationModel<BoardItem> {
    private Long id;
    private String name;
    private String user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
