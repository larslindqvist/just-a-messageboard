package se.kodaren.messageboard.api.index;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.kodaren.messageboard.api.index.model.Index;
import se.kodaren.messageboard.service.MessageboardService;

import java.security.Principal;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api")
public class IndexController {
    private final MessageboardService messageboardService;

    public IndexController(MessageboardService messageboardService) {
        this.messageboardService = messageboardService;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Index> getIndex(Principal principal) {
        Index index = messageboardService.getIndex(principal != null);
        return ResponseEntity.ok(index);
    }
}
