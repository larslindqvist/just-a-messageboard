package se.kodaren.messageboard.api.message.model;

import org.springframework.hateoas.RepresentationModel;

public class Message extends RepresentationModel<Message> {
    private Long id;
    private String message;
    private String user;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
