package se.kodaren.messageboard.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import se.kodaren.messageboard.api.board.converter.BoardConverter;
import se.kodaren.messageboard.api.board.model.Board;
import se.kodaren.messageboard.api.board.model.BoardPOST;
import se.kodaren.messageboard.api.board.model.Boards;
import se.kodaren.messageboard.api.index.model.Index;
import se.kodaren.messageboard.api.message.converter.MessageConverter;
import se.kodaren.messageboard.api.message.model.Message;
import se.kodaren.messageboard.api.message.model.MessagePOST;
import se.kodaren.messageboard.api.message.model.Messages;
import se.kodaren.messageboard.api.user.converter.UserConverter;
import se.kodaren.messageboard.api.user.model.User;
import se.kodaren.messageboard.api.user.model.UserPOST;
import se.kodaren.messageboard.api.util.LinkUtils;
import se.kodaren.messageboard.repository.*;
import se.kodaren.messageboard.service.exception.BoardNotFoundException;
import se.kodaren.messageboard.service.exception.MessageNotFoundException;
import se.kodaren.messageboard.service.exception.UserAlreadyExistsException;
import se.kodaren.messageboard.service.exception.UserNotFoundException;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class MessageboardService {
    private final UserRepository userRepository;
    private final BoardRepository boardRepository;
    private final MessageRepository messageRepository;

    private final BoardConverter boardConverter;
    private final MessageConverter messageConverter;
    private final UserConverter userConverter;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final LinkUtils linkUtils;

    public MessageboardService(UserRepository userRepository, BoardRepository boardRepository, MessageRepository messageRepository, BoardConverter boardConverter, MessageConverter messageConverter, UserConverter userConverter, BCryptPasswordEncoder bCryptPasswordEncoder, LinkUtils linkUtils) {
        this.userRepository = userRepository;
        this.boardRepository = boardRepository;
        this.messageRepository = messageRepository;
        this.boardConverter = boardConverter;
        this.messageConverter = messageConverter;
        this.userConverter = userConverter;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.linkUtils = linkUtils;
    }

    public Index getIndex(boolean authenticated) {
        Index index = new Index();
        linkUtils.addLinks(index, authenticated);
        return index;
    }

    public User createUser(UserPOST user) throws UserAlreadyExistsException {
        if (userRepository.existsByUsername(user.getUsername())) {
            throw new UserAlreadyExistsException("User already exists");
        }

        UserDB userDB = new UserDB();
        userDB.setUsername(user.getUsername());
        userDB.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        UserDB created = userRepository.save(userDB);
        User resource = userConverter.convert(created);
        linkUtils.addLinks(resource);
        return resource;
    }

    public Board createUserBoard(Long userId, BoardPOST board) throws UserNotFoundException {
        UserDB userDB = userRepository.getById(userId);
        if(userDB == null) {
            throw new UserNotFoundException("User does not exist.");
        }

        BoardDB boardDB = new BoardDB();
        boardDB.setName(board.getName());
        boardDB.setUser(userDB);

        BoardDB created = boardRepository.save(boardDB);
        Board resource = boardConverter.convert(created);
        linkUtils.addLinks(resource);
        return resource;
    }

    public Boards getBoards() {
        var boardList = StreamSupport.stream(boardRepository.findAll().spliterator(), false)
                .map(boardConverter::convertItem)
                .collect(Collectors.toList());
        Boards boards = new Boards();
        boards.setBoards(boardList);
        linkUtils.addLinks(boards);
        return boards;
    }

    public Board findBoard(Long id) throws BoardNotFoundException {
        BoardDB boardDB = boardRepository.getById(id);
        if (boardDB == null) {
            throw new BoardNotFoundException("Board does not exist.");
        }

        Board resource = boardConverter.convert(boardDB);
        linkUtils.addLinks(resource);
        return resource;
    }

    public Messages getMessages(Long boardId) {
        var board = boardRepository.getById(boardId);
        if (board == null) {
            throw new BoardNotFoundException("Board does not exist.");
        }

        var messageList = messageRepository.findByBoardId(boardId).stream()
                .map(messageConverter::convertItem)
                .collect(Collectors.toList());
        Messages messages = new Messages();
        messages.setMessageItems(messageList);
        linkUtils.addLinks(boardId, messages);
        return messages;
    }

    public User findUser(Long id) {
        UserDB userDB = userRepository.getById(id);
        if (userDB == null) {
            throw new UserNotFoundException("User does not exist.");
        }

        User resource = userConverter.convert(userDB);
        linkUtils.addLinks(resource);
        return resource;
    }

    public User findUser(String username) {
        UserDB userDB = userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException("User does not exist."));
        User resource = userConverter.convert(userDB);
        linkUtils.addLinks(resource);
        return resource;
    }

    public Message createMessage(Long boardId, Long userId, MessagePOST message) throws BoardNotFoundException, UserNotFoundException {
        BoardDB boardDB = boardRepository.getById(boardId);
        if (boardDB == null) {
            throw new BoardNotFoundException("Board does not exist.");
        }

        UserDB userDB = userRepository.getById(userId);
        if (userDB == null) {
            throw new UserNotFoundException("User does not exist");
        }

        MessageDB messageDB = new MessageDB();
        messageDB.setMessage(message.getMessage());
        messageDB.setBoard(boardDB);
        messageDB.setUser(userDB);

        MessageDB created = messageRepository.save(messageDB);
        Message resource = messageConverter.convert(created);
        linkUtils.addLinks(boardId, resource);
        return resource;
    }

    public Message findMessage(Long boardId, Long messageId) throws BoardNotFoundException, MessageNotFoundException {
        BoardDB boardDB = boardRepository.getById(boardId);
        if (boardDB == null) {
            throw new BoardNotFoundException("Board does not exist.");
        }

        MessageDB messageDB = messageRepository.findById(messageId).orElseThrow(() -> new MessageNotFoundException("Message does not exist."));
        Message resource = messageConverter.convert(messageDB);
        linkUtils.addLinks(boardId, resource);
        return resource;
    }

    public void deleteUser(Long userId) throws UserNotFoundException {
        var user = userRepository.getById(userId);
        if (user == null) {
            throw new UserNotFoundException("User does not exist.");
        }

        var boards = boardRepository.findByUserId(userId);
        for (var board : boards) {
            var messages = messageRepository.findByBoardId(board.getId());
            for (var message : messages) {
                messageRepository.delete(message);
            }

            boardRepository.delete(board);
        }

        userRepository.deleteById(userId);
    }

    public void updateMessage(Long boardId, Long messageId, MessagePOST message) throws MessageNotFoundException {
        var board = boardRepository.getById(boardId);
        if (board == null) {
            throw new BoardNotFoundException("Board does not exist.");
        }

        var messageOptional = messageRepository.findById(messageId);
        if (messageOptional.isPresent()) {
            messageOptional.get().setMessage(message.getMessage());
            messageRepository.save(messageOptional.get());
        }
        else {
            throw new MessageNotFoundException("Message does not exist.");
        }
    }

    public void deleteMessage(Long boardId, Long messageId) throws MessageNotFoundException {
        var board = boardRepository.getById(boardId);
        if (board == null) {
            throw new BoardNotFoundException("Board does not exist.");
        }

        var message = messageRepository.findById(messageId);
        if (message.isPresent()) {
            messageRepository.delete(message.get());
        }
        else {
            throw new MessageNotFoundException("Message does not exist.");
        }
    }
}
