package se.kodaren.messageboard.api.message;

import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import se.kodaren.messageboard.api.board.model.BoardPOST;
import se.kodaren.messageboard.api.message.model.MessagePOST;
import se.kodaren.messageboard.tests.BaseITCase;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MessageControllerITCase extends BaseITCase {
    @Test
    public void testHappyFlow() throws Exception {
        // Create a board
        var board = new BoardPOST();
        board.setName("A Board");
        var boardPOSTHttpEntity = new HttpEntity<>(board, FIRST_USER_HEADERS);
        var postBoardResponse = restTemplate.exchange(url("/api/boards"), HttpMethod.POST, boardPOSTHttpEntity, BoardPOST.class);
        var boardLocation = Objects.requireNonNull(postBoardResponse.getHeaders().get(HttpHeaders.LOCATION)).get(0);
        assertEquals(HttpStatus.CREATED, postBoardResponse.getStatusCode());

        // Create message
        var message = new MessagePOST();
        message.setMessage("My message");
        var messagePOSTHttpEntity = new HttpEntity<>(message, FIRST_USER_HEADERS);
        var postMessageResponse = restTemplate.exchange(boardLocation + "/messages", HttpMethod.POST, messagePOSTHttpEntity, MessagePOST.class);
        var messageLocation = Objects.requireNonNull(postMessageResponse.getHeaders().get(HttpHeaders.LOCATION)).get(0);
        assertEquals(HttpStatus.CREATED, postMessageResponse.getStatusCode());

        // Get message
        var getMessageResponse = restTemplate.exchange(messageLocation, HttpMethod.GET, new HttpEntity<>(null, FIRST_USER_HEADERS), String.class);
        var getMessageExpected = "{id:" + getIdFromPath(messageLocation) + ",message:'My message'}";
        assertEquals(HttpStatus.OK, getMessageResponse.getStatusCode());
        JSONAssert.assertEquals(getMessageExpected, getMessageResponse.getBody(), false);

        // Update message
        message.setMessage("Updated");
        var updateMessagePOSTHttpEntity = new HttpEntity<>(message, FIRST_USER_HEADERS);
        var putMessageResponse = restTemplate.exchange(messageLocation, HttpMethod.PUT, updateMessagePOSTHttpEntity, MessagePOST.class);
        assertEquals(HttpStatus.OK, putMessageResponse.getStatusCode());

        // Get message
        getMessageResponse = restTemplate.exchange(messageLocation, HttpMethod.GET, new HttpEntity<>(null, FIRST_USER_HEADERS), String.class);
        getMessageExpected = "{id:" + getIdFromPath(messageLocation) + ",message:'Updated'}";
        assertEquals(HttpStatus.OK, getMessageResponse.getStatusCode());
        JSONAssert.assertEquals(getMessageExpected, getMessageResponse.getBody(), false);

        // Get messages
        var getMessagesResponse = restTemplate.exchange(boardLocation + "/messages", HttpMethod.GET, new HttpEntity<>(null, FIRST_USER_HEADERS), String.class);
        var getMessagesExpected = "{_embedded: {messageItemList:[{id:" + getIdFromPath(messageLocation) + ",message:'Updated'}]}}";
        JSONAssert.assertEquals(getMessagesExpected, getMessagesResponse.getBody(), false);

        // Delete message
        var deleteMessageResponse = restTemplate.exchange(messageLocation, HttpMethod.DELETE, new HttpEntity<>(null, FIRST_USER_HEADERS), String.class);
        assertEquals(HttpStatus.NO_CONTENT, deleteMessageResponse.getStatusCode());
    }

    @Test
    public void testUpdateMessageForDifferentUser() {
        // Create a board
        var board = new BoardPOST();
        board.setName("A Board");
        var boardPOSTHttpEntity = new HttpEntity<>(board, FIRST_USER_HEADERS);
        var postBoardResponse = restTemplate.exchange(url("/api/boards"), HttpMethod.POST, boardPOSTHttpEntity, BoardPOST.class);
        var boardLocation = Objects.requireNonNull(postBoardResponse.getHeaders().get(HttpHeaders.LOCATION)).get(0);

        // Post a message as user #2
        var message = new MessagePOST();
        message.setMessage("Message");
        var messagePOSTHttpEntity = new HttpEntity<>(message, SECOND_USER_HEADERS);
        var postMessageResponse = restTemplate.exchange(boardLocation + "/messages", HttpMethod.POST, messagePOSTHttpEntity, MessagePOST.class);
        var messageLocation = Objects.requireNonNull(postMessageResponse.getHeaders().get(HttpHeaders.LOCATION)).get(0);

        // Try to update a message for a different user
        try {
            message.setMessage("Updated");
            var messagePOSTEntity = new HttpEntity<>(message, FIRST_USER_HEADERS);
            var response = restTemplate.exchange(messageLocation, HttpMethod.PUT, messagePOSTEntity, MessagePOST.class);
            assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        }
        catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
        }
    }

    @Test
    public void testDeleteMessageForDifferentUser() {
        // Create a board
        var board = new BoardPOST();
        board.setName("A Board");
        var boardPOSTHttpEntity = new HttpEntity<>(board, FIRST_USER_HEADERS);
        var postBoardResponse = restTemplate.exchange(url("/api/boards"), HttpMethod.POST, boardPOSTHttpEntity, BoardPOST.class);
        var boardLocation = Objects.requireNonNull(postBoardResponse.getHeaders().get(HttpHeaders.LOCATION)).get(0);

        // Post a message as user #2
        var message = new MessagePOST();
        message.setMessage("Message");
        var messagePOSTHttpEntity = new HttpEntity<>(message, SECOND_USER_HEADERS);
        var postMessageResponse = restTemplate.exchange(boardLocation + "/messages", HttpMethod.POST, messagePOSTHttpEntity, MessagePOST.class);
        var messageLocation = Objects.requireNonNull(postMessageResponse.getHeaders().get(HttpHeaders.LOCATION)).get(0);

        // Try to delete a message for a different user
        try {
            var response = restTemplate.exchange(messageLocation, HttpMethod.DELETE, new HttpEntity<>(null, FIRST_USER_HEADERS), MessagePOST.class);
            assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        }
        catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
        }
    }

    @Test
    public void testGetMessagesOfNonExistingBoard() {
        // Try to get messages of a non-existing board
        try {
            var response = restTemplate.exchange(url("/api/boards/100/messages"), HttpMethod.GET, new HttpEntity<>(null, FIRST_USER_HEADERS), String.class);
            assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        }
        catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
        }
    }

    @Test
    public void testGetNonExistingMessage() {
        // Create a board
        var board = new BoardPOST();
        board.setName("A Board");
        var boardPOSTHttpEntity = new HttpEntity<>(board, FIRST_USER_HEADERS);
        var postBoardResponse = restTemplate.exchange(url("/api/boards"), HttpMethod.POST, boardPOSTHttpEntity, BoardPOST.class);
        var boardLocation = Objects.requireNonNull(postBoardResponse.getHeaders().get(HttpHeaders.LOCATION)).get(0);

        // Try to get a non-existing message
        try {
            var response = restTemplate.exchange(boardLocation + "/messages/300", HttpMethod.GET, new HttpEntity<>(null, FIRST_USER_HEADERS), String.class);
            assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        }
        catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
        }
    }

    @Test
    public void testCreateMessageOnNonExistingBoard() {
        // Try to create a message on non-existing board
        var message = new MessagePOST();
        message.setMessage("Message");
        var messagePOSTHttpEntity = new HttpEntity<>(message, FIRST_USER_HEADERS);

        try {
            var response = restTemplate.exchange(url( "/api/boards/200/messages"), HttpMethod.POST, messagePOSTHttpEntity, MessagePOST.class);
            assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        }
        catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
        }
    }

    @Test
    public void testUpdateMessageOnNonExistingBoard() {
        // Try to update a message on non-existing board
        var message = new MessagePOST();
        message.setMessage("Message");
        var messagePOSTHttpEntity = new HttpEntity<>(message, FIRST_USER_HEADERS);

        try {
            var response = restTemplate.exchange(url( "/api/boards/201/messages/400"), HttpMethod.PUT, messagePOSTHttpEntity, MessagePOST.class);
            assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        }
        catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
        }
    }

    @Test
    public void testUpdateNonExistingMessage() {
        // Create a board
        var board = new BoardPOST();
        board.setName("A Board");
        var boardPOSTHttpEntity = new HttpEntity<>(board, FIRST_USER_HEADERS);
        var postBoardResponse = restTemplate.exchange(url("/api/boards"), HttpMethod.POST, boardPOSTHttpEntity, BoardPOST.class);
        var boardLocation = Objects.requireNonNull(postBoardResponse.getHeaders().get(HttpHeaders.LOCATION)).get(0);

        // Try to update a non-existing message on existing board
        var message = new MessagePOST();
        message.setMessage("Message");
        var messagePOSTHttpEntity = new HttpEntity<>(message, FIRST_USER_HEADERS);

        try {
            var response = restTemplate.exchange(boardLocation + "/messages/300", HttpMethod.PUT, messagePOSTHttpEntity, MessagePOST.class);
            assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        }
        catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
        }
    }

    @Test
    public void testDeleteNonExistingMessage() {
        // Create a board
        var board = new BoardPOST();
        board.setName("A Board");
        var boardPOSTHttpEntity = new HttpEntity<>(board, FIRST_USER_HEADERS);
        var postBoardResponse = restTemplate.exchange(url("/api/boards"), HttpMethod.POST, boardPOSTHttpEntity, BoardPOST.class);
        var boardLocation = Objects.requireNonNull(postBoardResponse.getHeaders().get(HttpHeaders.LOCATION)).get(0);

        // Try to delete a non-existing message on board
        try {
            var response = restTemplate.exchange(boardLocation + "/messages/300", HttpMethod.DELETE, new HttpEntity<>(null, FIRST_USER_HEADERS), MessagePOST.class);
            assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        }
        catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
        }
    }
}
