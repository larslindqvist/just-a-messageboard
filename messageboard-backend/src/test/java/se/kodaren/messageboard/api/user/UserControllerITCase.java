package se.kodaren.messageboard.api.user;

import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import se.kodaren.messageboard.tests.BaseITCase;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerITCase extends BaseITCase {
    @Test
    public void testHappyFlow() throws Exception {
        // Get user
        var getUserResponse = restTemplate.exchange(FIRST_USER, HttpMethod.GET, new HttpEntity<>(null, FIRST_USER_HEADERS), String.class);
        var getUserExpected = "{id: "+ getIdFromPath(FIRST_USER) + ",username:testuser1}";
        JSONAssert.assertEquals(getUserExpected, getUserResponse.getBody(), false);
    }

    @Test
    public void testGetDifferentUser() {
        // Try to get different user
        try {
            var response = restTemplate.exchange(SECOND_USER, HttpMethod.GET, new HttpEntity<>(null, FIRST_USER_HEADERS), String.class);
            assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        }
        catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
        }
    }

    @Test
    public void testGetNonExistingUser() {
        // Try to get a non-existing user
        try {
            var response = restTemplate.exchange(url("/api/users/200"), HttpMethod.GET, new HttpEntity<>(null, FIRST_USER_HEADERS), String.class);
            assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        }
        catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
        }
    }

    @Test
    public void testDeleteDifferentUser() {
        // Try to delete a different user
        try {
            var response = restTemplate.exchange(SECOND_USER, HttpMethod.DELETE, new HttpEntity<>(null, FIRST_USER_HEADERS), String.class);
            assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        }
        catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
        }
    }

    @Test
    public void testDeleteNonExistingUser() {
        // Try to delete a non-existing user
        try {
            var response = restTemplate.exchange(url("/api/users/200"), HttpMethod.DELETE, new HttpEntity<>(null, FIRST_USER_HEADERS), String.class);
            assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        }
        catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
        }
    }
}
