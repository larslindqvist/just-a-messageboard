package se.kodaren.messageboard.api.board.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.kodaren.messageboard.api.board.model.Board;
import se.kodaren.messageboard.repository.BoardDB;
import se.kodaren.messageboard.repository.UserDB;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BoardConverterTestCase {
    private final BoardConverter boardConverter = new BoardConverter();

    private BoardDB BOARD = new BoardDB();

    @BeforeEach
    public void setUp() {
        UserDB USER = new UserDB();
        USER.setUsername("username1");

        BOARD = new BoardDB();
        BOARD.setUser(USER);
        BOARD.setName("board1");
    }

    @Test
    public void testConvertToBoard() {
        // Given a populated BoardDB

        // When converting to the API model
        Board board = boardConverter.convert(BOARD);

        // Verify the API contents
        assertEquals(BOARD.getId(), board.getId());
        assertEquals(BOARD.getName(), board.getName());
        assertEquals(BOARD.getUser().getUsername(), board.getUser());
    }

    @Test
    public void testConvertToBoardItem() {
        // Given a populated BoardDB

        // When converting to the API model
        Board board = boardConverter.convert(BOARD);

        // Verify the API contents
        assertEquals(BOARD.getId(), board.getId());
        assertEquals(BOARD.getName(), board.getName());
        assertEquals(BOARD.getUser().getUsername(), board.getUser());
    }
}
