package se.kodaren.messageboard.api.user.converter;

import org.junit.jupiter.api.Test;
import se.kodaren.messageboard.api.user.model.User;
import se.kodaren.messageboard.repository.UserDB;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserConverterTestCase {
    private final UserConverter userConverter = new UserConverter();

    @Test
    public void testConvertUserDB() {
        // Given a populated UserDB
        UserDB USER = new UserDB();
        USER.setUsername("username1");

        // When converting to the API model
        User user = userConverter.convert(USER);

        // Verify the API contents
        assertEquals(USER.getId(), user.getId());
        assertEquals(USER.getUsername(), user.getUsername());
    }
}
