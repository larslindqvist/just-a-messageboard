package se.kodaren.messageboard.api.user;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import se.kodaren.messageboard.api.message.MessageController;
import se.kodaren.messageboard.tests.BaseTestCase;

@WebMvcTest(controllers = MessageController.class)
@WithMockUser
public class UserControllerTestCase extends BaseTestCase {
}
