package se.kodaren.messageboard.api.message.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.kodaren.messageboard.api.message.model.Message;
import se.kodaren.messageboard.api.message.model.MessageItem;
import se.kodaren.messageboard.repository.BoardDB;
import se.kodaren.messageboard.repository.MessageDB;
import se.kodaren.messageboard.repository.UserDB;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MessageConverterTestCase {
    private final MessageConverter messageConverter = new MessageConverter();

    private MessageDB MESSAGE = new MessageDB();

    @BeforeEach
    public void setUp() {
        UserDB USER = new UserDB();
        USER.setUsername("username1");

        BoardDB BOARD = new BoardDB();
        BOARD.setUser(USER);
        BOARD.setName("board1");

        MESSAGE = new MessageDB();
        MESSAGE.setMessage("message1");
        MESSAGE.setUser(USER);
        MESSAGE.setBoard(BOARD);
    }

    @Test
    public void testConvertToMessage() {
        // Given a populated MessageDB

        // When converting to the API model
        Message message = messageConverter.convert(MESSAGE);

        // Verify the API contents
        assertEquals(MESSAGE.getId(), message.getId());
        assertEquals(MESSAGE.getMessage(), message.getMessage());
        assertEquals(MESSAGE.getUser().getUsername(), message.getUser());
    }

    @Test
    public void testConvertToMessageItem() {
        // Given a populated MessageDB

        // When converting to the API model
        MessageItem message = messageConverter.convertItem(MESSAGE);

        // Verify the API contents
        assertEquals(MESSAGE.getId(), message.getId());
        assertEquals(MESSAGE.getMessage(), message.getMessage());
        assertEquals(MESSAGE.getUser().getUsername(), message.getUser());
    }
}
