package se.kodaren.messageboard.api.board;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import se.kodaren.messageboard.api.board.model.BoardPOST;
import se.kodaren.messageboard.tests.BaseTestCase;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = BoardController.class)
@WithMockUser
public class BoardControllerTestCase extends BaseTestCase {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testPostNullBoard() throws Exception {
        var board = new BoardPOST();
        board.setName(null);
        mockMvc.perform(post("/api/boards").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
    }

    @Test
    public void testPostEmptyBoard() throws Exception {
        var board = new BoardPOST();
        board.setName("   ");
        mockMvc.perform(post("/api/boards").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
    }
}
