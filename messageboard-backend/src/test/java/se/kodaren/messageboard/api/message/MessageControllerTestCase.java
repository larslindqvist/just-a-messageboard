package se.kodaren.messageboard.api.message;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import se.kodaren.messageboard.api.message.model.MessagePOST;
import se.kodaren.messageboard.tests.BaseTestCase;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = MessageController.class)
@WithMockUser
public class MessageControllerTestCase extends BaseTestCase {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testPostNullMessage() throws Exception {
        var message = new MessagePOST();
        message.setMessage(null);
        mockMvc.perform(post("/api/boards/1/messages").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
    }

    @Test
    public void testPostEmptyMessage() throws Exception {
        var message = new MessagePOST();
        message.setMessage("  ");
        mockMvc.perform(post("/api/boards/1/messages").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
    }

    @Test
    public void testPostNullOwner() throws Exception {
        var message = new MessagePOST();
        message.setMessage("test");
        mockMvc.perform(post("/api/boards/1/messages").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
    }

    @Test
    public void testPostNegativeOwner() throws Exception {
        var message = new MessagePOST();
        message.setMessage("test");
        mockMvc.perform(post("/api/boards/1/messages").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
    }

    @Test
    public void testPutNullMessage() throws Exception {
        var message = new MessagePOST();
        message.setMessage(null);
        mockMvc.perform(put("/api/boards/1/messages/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
    }

    @Test
    public void testPutEmptyMessage() throws Exception {
        var message = new MessagePOST();
        message.setMessage("  ");
        mockMvc.perform(put("/api/boards/1/messages/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
    }
}
