package se.kodaren.messageboard.api.board;

import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import se.kodaren.messageboard.api.board.model.BoardPOST;
import se.kodaren.messageboard.tests.BaseITCase;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BoardControllerITCase extends BaseITCase {
    @Test
    public void testHappyFlow() throws Exception {
        // Create a board
        var userId = getIdFromPath(FIRST_USER);
        var board = new BoardPOST();
        board.setName("A Board");
        var boardPOSTHttpEntity = new HttpEntity<>(board, FIRST_USER_HEADERS);
        var postBoardResponse = restTemplate.exchange(url("/api/boards"), HttpMethod.POST, boardPOSTHttpEntity, BoardPOST.class);
        var boardLocation = Objects.requireNonNull(postBoardResponse.getHeaders().get(HttpHeaders.LOCATION)).get(0);
        assertEquals(HttpStatus.CREATED, postBoardResponse.getStatusCode());

        // Get board
        var getBoardResponse = restTemplate.exchange(boardLocation, HttpMethod.GET, new HttpEntity<>(null, FIRST_USER_HEADERS), String.class);
        var getBoardExpected = "{id:" + getIdFromPath(boardLocation) + ",name:'A Board'}";
        JSONAssert.assertEquals(getBoardExpected, getBoardResponse.getBody(), false);

        // Get boards
        var getBoardsResponse = restTemplate.exchange(url("/api/boards"), HttpMethod.GET, new HttpEntity<>(null, FIRST_USER_HEADERS), String.class);
        var getBoardsExpected = "{_embedded: { boardItemList:[{id:" + getIdFromPath(boardLocation) + ",name:'A Board'}]}}";
        assertEquals(HttpStatus.OK, getBoardsResponse.getStatusCode());
        JSONAssert.assertEquals(getBoardsExpected, getBoardsResponse.getBody(), false);
    }

    @Test
    public void testGetNonExistingBoard() {
        // Try to get a non-existing board
        try {
            var response = restTemplate.exchange(url("/api/boards/100"), HttpMethod.GET, new HttpEntity<>(null, FIRST_USER_HEADERS), String.class);
            assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        }
        catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
        }
    }

    @Test
    public void testCreateBoardForNonExistantUser() {
        // Try to create a board for a different user
        try {
            var board = new BoardPOST();
            board.setName("My Board");
            var boardPOSTHttpEntity = new HttpEntity<>(board, FIRST_USER_HEADERS);
            var response = restTemplate.exchange(url("/boards"), HttpMethod.POST, boardPOSTHttpEntity, BoardPOST.class);
            assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        }
        catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
        }
    }
}
