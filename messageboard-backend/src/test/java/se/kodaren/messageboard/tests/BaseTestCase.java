package se.kodaren.messageboard.tests;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import se.kodaren.messageboard.api.util.LinkUtils;
import se.kodaren.messageboard.repository.UserRepository;
import se.kodaren.messageboard.security.UserDetailsServiceImpl;
import se.kodaren.messageboard.service.MessageboardService;

public class BaseTestCase {
    @MockBean
    protected MessageboardService messageboardService;

    @MockBean
    private LinkUtils linkUtils;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private BCryptPasswordEncoder bCryptPasswordEncoder;
}
