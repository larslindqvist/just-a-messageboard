package se.kodaren.messageboard.tests;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import se.kodaren.messageboard.api.user.model.UserPOST;

import java.util.Collections;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class BaseITCase {
    @LocalServerPort
    private int port;

    protected RestTemplate restTemplate = new RestTemplate();
    protected HttpHeaders FIRST_USER_HEADERS = new HttpHeaders();
    protected String FIRST_USER;
    protected String FIRST_USER_TOKEN;

    protected HttpHeaders SECOND_USER_HEADERS = new HttpHeaders();
    protected String SECOND_USER;
    protected String SECOND_USER_TOKEN;

    @BeforeEach
    public void setUp() {
        FIRST_USER_HEADERS.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        SECOND_USER_HEADERS.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        // Create a user
        var user = new UserPOST();
        user.setUsername("testuser1");
        user.setPassword("testpassword1");
        var userRegisterRequest = new HttpEntity<>(user, FIRST_USER_HEADERS);
        var userRegisterResponse = restTemplate.exchange(url("/api/register"), HttpMethod.POST, userRegisterRequest, UserPOST.class);
        FIRST_USER = Objects.requireNonNull(userRegisterResponse.getHeaders().get(HttpHeaders.LOCATION)).get(0);
        assertEquals(HttpStatus.CREATED, userRegisterResponse.getStatusCode());

        // Login user
        var userLoginRequest = new HttpEntity<>(user, FIRST_USER_HEADERS);
        var userLoginResponse = restTemplate.exchange(url("/api/login"), HttpMethod.POST, userLoginRequest, UserPOST.class);
        FIRST_USER_TOKEN = Objects.requireNonNull(userLoginResponse.getHeaders().get(HttpHeaders.AUTHORIZATION)).get(0);
        assertNotNull(FIRST_USER_TOKEN);
        // Save auth token for tests
        FIRST_USER_HEADERS.setBearerAuth(FIRST_USER_TOKEN);

        // Create a second user
        user = new UserPOST();
        user.setUsername("different-user-" + System.currentTimeMillis());
        user.setPassword("testpassword1");
        userRegisterRequest = new HttpEntity<>(user, FIRST_USER_HEADERS);
        userRegisterResponse = restTemplate.exchange(url("/api/register"), HttpMethod.POST, userRegisterRequest, UserPOST.class);
        SECOND_USER = Objects.requireNonNull(userRegisterResponse.getHeaders().get(HttpHeaders.LOCATION)).get(0);
        assertEquals(HttpStatus.CREATED, userRegisterResponse.getStatusCode());

        // Login user
        userLoginRequest = new HttpEntity<>(user, FIRST_USER_HEADERS);
        userLoginResponse = restTemplate.exchange(url("/api/login"), HttpMethod.POST, userLoginRequest, UserPOST.class);
        SECOND_USER_TOKEN = Objects.requireNonNull(userLoginResponse.getHeaders().get(HttpHeaders.AUTHORIZATION)).get(0);
        assertNotNull(SECOND_USER_TOKEN);
        // Save auth token for tests
        SECOND_USER_HEADERS.setBearerAuth(SECOND_USER_TOKEN);
    }

    @AfterEach
    public void tearDown() {
        // Delete user
        var userDeleteRequest = new HttpEntity<>(null, FIRST_USER_HEADERS);
        var userDeleteResponse = restTemplate.exchange(FIRST_USER, HttpMethod.DELETE, userDeleteRequest, String.class);
        assertEquals(HttpStatus.NO_CONTENT, userDeleteResponse.getStatusCode());
    }

    protected String url(String path) {
        return "http://localhost:" + port + path;
    }

    protected String url() {
        return "http://localhost:" + port;
    }

    protected Long getIdFromPath(String path) throws NumberFormatException {
        var segments = path.split("/");
        return Long.parseLong(segments[segments.length - 1]);
    }
}
