module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  devServer: {
    proxy: {
      '^/api': {
        target: process.env.BACKEND_URL,
        changeOrigin: true,
      },
    },
  }
}
