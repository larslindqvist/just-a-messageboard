import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/boards',
    name: 'boards',
    component: () => import(/* webpackChunkName: "about" */ '../views/Boards.vue')
  },
  {
    path: '/boards/:id',
    name: 'board',
    component: () => import(/* webpackChunkName: "about" */ '../views/Board.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
