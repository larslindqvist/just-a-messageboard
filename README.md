# Just a Messageboard
## Description
In Just a Messageboard, or "JAM", users can create and view message boards, and post messages to them.
By design, users can only do these things if they have signed up and logged in.

JAM consists of two components: messageboard-backend and messageboard-frontend.

![List boards Screenshot](docs/img/messageboard-frontend-boards.png)

![View board Screenshot](docs/img/messageboard-frontend-board.png)

## Technologies
messageboard-backend uses various Spring technologies, such as Spring Boot, Security, Web, HATEOAS and Validation. The database manager used is h2.
Testing is done primarily with JUnit and Mockito. JSON responses are tested with [JSONassert](https://github.com/skyscreamer/JSONassert).

messageboard-frontend uses Vue CLI and Vuetify.

## Building
### Requirements
The requirements can be taken with a pinch of salt. It is possible that with minor adjustments the project can be built with different versions of the software listed. It can be said though that the listed requirements should work.

#### messageboard-backend:
- Maven 3.6.3
- JDK 15

#### messageboard-frontend:
- NodeJS 15.6.0
- npm 7.4.0

#### Docker and docker-compose (optional)
messageboard-backend can be built into a Docker image.
- Docker 20.10.2
- docker-compose 1.27.4

### How to build
messageboard-backend is built with Maven.
Invoking `mvn clean package` in the project root will run unit tests and build a runnable JAR in the target folder.

messageboard-frontend is built with NPM. See the Running section below.

#### Building a Docker image
The Docker image build process of messageboard-backend is integrated with Maven. Running `mvn clean package -Pdocker` in the project root or module directory will produce a docker image called kodaren/messageboard-backend.

## Running
messageboard-backend is run depending on which build method was used.
There is a docker-compose.yml file In the project root which sets up a container from the previously built messageboard-backend image when running `docker-compose up`.
Otherwise, running the JAR directory using `java -jar target/messageboard-backend-0.0.1-SNAPSHOT.jar` should also work.
Whichever method is used, messageboard-backend can be accessed on localhost:8080.
When running the JAR, you have the Spring profile `dev` available to use. This profile will use a file-based storage for h2. The docker image as-is will be in-memory.

To run messageboard-frontend, invoke `npm run serve` from the module directory. This will build and start the frontend locally.
